<!--
Format with an empty configuration file to see the effect of
using the built-in defaults.  The defaults are suitable for
data-oriented XML documents (shown by the <table> below),
but less so for mixed-mode content (shown by the <para> below).
For mixed-mode content, no newlines are added around text,
so sub-elements don't begin new lines the way they do for
data-oriented content.
-->
<root>
 <table>
  <row>
   <cell>1-1</cell>
   <cell>1-2</cell>
  </row>
  <row>
   <cell>2-1</cell>
   <cell>2-2</cell>
  </row>
  <row>
   <cell>3-1</cell>
   <cell>3-2</cell>
  </row>
 </table>
 <para>
This is a paragraph with <literal>literal</literal>
text and <emphasis>emphasized</emphasis> text.
</para>
</root>
