<!--
Test to show that wrap-length of inline element text is controlled
by the enclosing block (not by the enclosing inline element).  The
config file set a long wrap-length for para and a short wrap-length
for inline.  The comment within the inline forces a wrap-and-flush
to occur when it is seen, which tests whether the wrap-length for
the para (the current block) or the inline (the current element)
get used.  xmlformat should ignore the inline wrap-length in
preference for the para wrap-length.
-->

<para>This is a normalized paragraph <inline> with a long long<!-- comment -->long long inline </inline> element.</para>
