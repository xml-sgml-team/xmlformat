<!--
Test text normalization of para that:
- Does not contain sub-elements
- Contains inline sub-elements
- Contains non-normalized block sub-elements
- Contains embedded comments
Note: line-wrapping and indenting should be 0 for test.
normalization of para should be on, and programlisting should be verbatim
-->
<para>This text should have all surrounding whitespace removed</para>
<para>This text should have leading/trailing <literal>whitespace</literal> removed, but leave a space around the literal element></para>
<para>This text should have leading/trailing whitespace removed
<programlisting>
and also spaces adjacent
</programlisting>
to the programlisting.</para>
<para>The whitespace around
<!-- this comment -->
should not be removed entirely.</para>
