<!--
itemizedlist should be indented within para.
programlisting should be flush left
-->

<para>
  <itemizedlist/>
</para>

<para>
  <itemizedlist></itemizedlist>
</para>

<para>
<programlisting/>
</para>

<para>
<programlisting></programlisting>
</para>
