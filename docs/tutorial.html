<html><head><meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"><title>
xmlformat Tutorial
</title><meta name="generator" content="DocBook XSL Stylesheets V1.69.1"></head><body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF"><div class="article" lang="en"><div class="titlepage"><div><div><h2 class="title"><a name="id755641"></a>
<span><strong class="command">xmlformat</strong></span> Tutorial
</h2></div><div><div class="author"><h3 class="author"><span class="firstname">Paul</span> <span class="surname">DuBois</span></h3><code class="email">&lt;<a href="mailto:paul@kitebird.com">paul@kitebird.com</a>&gt;</code></div></div></div><hr></div><div class="toc"><p><b>Table of Contents</b></p><dl><dt><span class="sect1"><a href="#introduction">1. 
Introduction
</a></span></dt><dt><span class="sect1"><a href="#formatting-document">2. 
Formatting a Document
</a></span></dt><dt><span class="sect1"><a href="#using-config-file">3. 
Using a Configuration File
</a></span></dt><dt><span class="sect1"><a href="#inherited-formatting">4. 
Discovering "Inherited" Formatting Options
</a></span></dt><dt><span class="sect1"><a href="#unconfigured-elements">5. 
Checking for Unconfigured Elements
</a></span></dt></dl></div><div class="sect1" lang="en"><div class="titlepage"><div><div><h2 class="title" style="clear: both"><a name="introduction"></a>1. 
Introduction
</h2></div></div></div><p>
This document is a user guide that provides a tutorial introduction to
the <span><strong class="command">xmlformat</strong></span> program. Another document,
<em class="citetitle">The <span><strong class="command">xmlformat</strong></span> Document
Formatter</em>, describes the capabilities of
<span><strong class="command">xmlformat</strong></span> in more detail.
</p></div><div class="sect1" lang="en"><div class="titlepage"><div><div><h2 class="title" style="clear: both"><a name="formatting-document"></a>2. 
Formatting a Document
</h2></div></div></div><p>
Suppose you have an XML document named <code class="filename">doc1.xml</code>
that looks like this:
</p><pre class="screen">
&lt;event&gt;
&lt;description&gt;I bought a new coffee cup!&lt;/description&gt;
&lt;date&gt;&lt;year&gt;2004&lt;/year&gt;&lt;month&gt;2&lt;/month&gt;&lt;day&gt;1&lt;/day&gt;&lt;/date&gt;
&lt;/event&gt;
</pre><p>
Suppose further that you want it to look like this:
</p><pre class="screen">
&lt;event&gt;
 &lt;description&gt;I bought a new coffee cup!&lt;/description&gt;
 &lt;date&gt;
  &lt;year&gt;2004&lt;/year&gt;
  &lt;month&gt;2&lt;/month&gt;
  &lt;day&gt;1&lt;/day&gt;
 &lt;/date&gt;
&lt;/event&gt;
</pre><p>
By happy coincidence, that happens to be exactly the default output
style produced by <span><strong class="command">xmlformat</strong></span>. To reformat your
document, all you have to do is run <span><strong class="command">xmlformat</strong></span> with
the document filename as the argument, saving the output in another
file:
</p><pre class="screen">
% <strong class="userinput"><code>xmlformat doc1.xml &gt; output</code></strong>
</pre><p>
Note: <code class="literal">%</code> represents your shell prompt; do not type it
as part of the command.
</p><p>
If you are confident that the output style produced by
<span><strong class="command">xmlformat</strong></span> will be as you desire, you can be reckless
and perform an in-place conversion:
</p><pre class="screen">
% <strong class="userinput"><code>xmlformat -i doc1.xml</code></strong>
</pre><p>
In this case, <span><strong class="command">xmlformat</strong></span> reads the document from the
input file, reformats it, and writes it back out to the same file,
replacing the file's original contents. If you are not quite so
reckless, use <code class="option">-i</code> in conjunction with a
<code class="option">-b</code> option to make a backup file that contains the
original document. <code class="option">-b</code> takes an argument that specifies
the suffix to add to the original filename to create the backup
filename. For example, to back up the original
<code class="filename">doc1.xml</code> file in a file named
<code class="filename">doc1.xml.bak</code>, use this command:
</p><pre class="screen">
% <strong class="userinput"><code>xmlformat -i -b .bak doc1.xml</code></strong>
</pre></div><div class="sect1" lang="en"><div class="titlepage"><div><div><h2 class="title" style="clear: both"><a name="using-config-file"></a>3. 
Using a Configuration File
</h2></div></div></div><p>
In the preceding example, the desired output style for
<code class="filename">doc1.xml</code> was the same as what
<span><strong class="command">xmlformat</strong></span> produces by default. But what if the
default style is <span class="emphasis"><em>not</em></span> what you want? In that case,
you must tell <span><strong class="command">xmlformat</strong></span> how to handle your document.
This is at once both the weakness and strength of
<span><strong class="command">xmlformat</strong></span>. The weakness is that it is extra work to
instruct <span><strong class="command">xmlformat</strong></span> how you want it to format a
document. The strength is that it's possible to do so. Other XML
formatters do not require any extra work, but that's because they are
not configurable.
</p><p>
Suppose <code class="filename">doc2.xml</code> looks like this:
</p><pre class="screen">
&lt;example&gt;&lt;title&gt;Compiling and Running a Program&lt;/title&gt;
&lt;para&gt;To compile and run the program,
use the following commands, where
&lt;replaceable&gt;source-file&lt;/replaceable&gt;
is the name of the source file:&lt;/para&gt;&lt;screen&gt;
&lt;userinput&gt;cc&lt;/userinput&gt; &lt;replaceable&gt;source-file&lt;/replaceable&gt;
&lt;userinput&gt;./a.out&lt;/userinput&gt;
&lt;/screen&gt;
&lt;/example&gt;
</pre><p>
That's ugly, and you want it to rewrite it like this:
</p><pre class="screen">
&lt;example&gt;

&lt;title&gt;Compiling and Running a Program&lt;/title&gt;

&lt;para&gt;
 To compile and run the program, use the following commands,
 where &lt;replaceable&gt;source-file&lt;/replaceable&gt; is the name of
 the source file:
&lt;/para&gt;

&lt;screen&gt;
&lt;userinput&gt;cc&lt;/userinput&gt; &lt;replaceable&gt;source-file&lt;/replaceable&gt;
&lt;userinput&gt;./a.out&lt;/userinput&gt;
&lt;/screen&gt;

&lt;/example&gt;
</pre><p>
The key characteristics of this rewrite are as follows:
</p><div class="itemizedlist"><ul type="disc"><li><p>
Child elements of the <code class="literal">&lt;example&gt;</code> element are
separated by blank lines, but not indented within it.
</p></li><li><p>
The text inside the <code class="literal">&lt;para&gt;</code> element is
reformatted, adjusted to 60 characters per line and indented.
</p></li><li><p>
The contents of the <code class="literal">&lt;screen&gt;</code> element are left
alone.
</p></li></ul></div><p>
Unfortunately, if you run <code class="filename">doc2.xml</code> through
<span><strong class="command">xmlformat</strong></span>, it comes out like this:
</p><pre class="screen">
&lt;example&gt;
 &lt;title&gt;Compiling and Running a Program&lt;/title&gt;
 &lt;para&gt;To compile and run the program,
use the following commands, where
&lt;replaceable&gt;source-file&lt;/replaceable&gt;
is the name of the source file:&lt;/para&gt;
 &lt;screen&gt;
  &lt;userinput&gt;cc&lt;/userinput&gt;
  &lt;replaceable&gt;source-file&lt;/replaceable&gt;
  &lt;userinput&gt;./a.out&lt;/userinput&gt;
 &lt;/screen&gt;
&lt;/example&gt;
</pre><p>
This output is unsuitable. Among the offenses committed by
<span><strong class="command">xmlformat</strong></span>, two are most notable:
</p><div class="itemizedlist"><ul type="disc"><li><p>
The text of the <code class="literal">&lt;para&gt;</code> element has been left
alone, not reformatted.
</p></li><li><p>
The <code class="literal">&lt;screen&gt;</code> element content has been
reformatted, not left intact.
</p></li></ul></div><p>
In these respects, it appears that <span><strong class="command">xmlformat</strong></span> has done
exactly the <span class="emphasis"><em>opposite</em></span> of what was wanted!
Furthermore, had you used the <code class="option">-i</code> option to reformat the
file in place without using <code class="option">-b</code> to make a backup, at
this point you would have a file containing a
<code class="literal">&lt;screen&gt;</code> element that you'd have to fix up by
hand to restore it to its original condition.
</p><p>
What a worthless, worthless program!
</p><p>
The rewriting of the <code class="literal">&lt;screen&gt;</code> element points to
an important lesson: Before trusting <span><strong class="command">xmlformat</strong></span> with
your documents, it's best to run some tests and tune your configuration
as necessary to make sure it will produce the results you want.
Otherwise, you may produce changes that affect the integrity of your
documents. This is particularly true when they contain elements such as
<code class="literal">&lt;screen&gt;</code> or
<code class="literal">&lt;programlisting&gt;</code> that should be copied
verbatim, without change.
</p><p>
Configuring <span><strong class="command">xmlformat</strong></span> amounts to writing a
configuration file that instructs it what to do. For
<code class="filename">doc2.xml</code>, that means telling
<span><strong class="command">xmlformat</strong></span> to leave the
<code class="literal">&lt;screen&gt;</code> element alone, to normalize the text
of the paragraph to fill lines and wrap them to a given length, and to
put blank lines around sub-elements of the
<code class="literal">&lt;example&gt;</code> element.
</p><p>
Let's begin by creating a very basic configuration file. What should we
call it? <span><strong class="command">xmlformat</strong></span> can read configuration settings
from a file named on the command line with a <code class="option">-f</code> or
<code class="option">--config-file</code> option. This means you can name the file
whatever you want. However, if you put the settings in a file named
<code class="filename">xmlformat.conf</code> in the current directory,
<span><strong class="command">xmlformat</strong></span> will read the file automatically. That's an
easier approach, because you won't need to use a command-line option to
specify the configuration file. So create a file named
<code class="filename">xmlformat.conf</code> that contains the following two
lines:
</p><pre class="screen">
screen
  format = verbatim
</pre><p>
These lines specify that <code class="literal">&lt;screen&gt;</code> elements
should be formatted as verbatim elements. That is,
<span><strong class="command">xmlformat</strong></span> should reproduce their content in the
output exactly as it appears in the input, without modification. The
first line must begin in column 1 (no preceding spaces or tabs). The
second line must begin with at least one space or tab. Presence or
absence of whitespace is how <span><strong class="command">xmlformat</strong></span> distinguish
the names of elements to be formatted from the instructions that
indicate <span class="emphasis"><em>how</em></span> to format them.
</p><p>
After creating <code class="filename">xmlformat.conf</code>, run
<span><strong class="command">xmlformat</strong></span> again to process
<code class="filename">doc2.xml</code>. It reads the newly created configuration
file and produces this result:
</p><pre class="screen">
&lt;example&gt;
 &lt;title&gt;Compiling and Running a Program&lt;/title&gt;
 &lt;para&gt;To compile and run the program,
use the following commands, where
&lt;replaceable&gt;source-file&lt;/replaceable&gt;
is the name of the source file:&lt;/para&gt;
&lt;screen&gt;
&lt;userinput&gt;cc&lt;/userinput&gt; &lt;replaceable&gt;source-file&lt;/replaceable&gt;
&lt;userinput&gt;./a.out&lt;/userinput&gt;
&lt;/screen&gt;
&lt;/example&gt;
</pre><p>
That's a little better: <span><strong class="command">xmlformat</strong></span> has not destroyed
the <code class="literal">&lt;screen&gt;</code> element by reformatting it. But
problems remain: The paragraph content has not been reformatted, and
there are no blank lines between sub-elements.
</p><p>
Let's take care of the paragraph next. To set up its formatting, add a
section to <code class="filename">xmlformat.conf</code> for
<code class="literal">&lt;para&gt;</code> elements:
</p><pre class="screen">
para
  format = block
  normalize = yes
  wrap-length = 60
  subindent = 1

screen
  format = verbatim
</pre><p>
The order of sections in the configuration file doesn't matter. Put them
in the order that makes most sense to you. The order of option lines
under the initial section line doesn't matter, either.
</p><p>
The first two options in the <code class="literal">para</code> section specify
that the <code class="literal">&lt;para&gt;</code> element is a block element, and
that text within it should be normalized. Turning on the
<code class="literal">normalize</code> option tells <span><strong class="command">xmlformat</strong></span>
that it's okay to reformat the text within the element. This means that
runs of whitespace within the text are collapsed to single spaces, and
that whitespace at the beginning and end of the text can be adjusted
(typically to put the text on different lines than the element's opening
and closing tags). Enabling normalization also allows you to perform
text line-wrapping and indenting. The <code class="literal">wrap-length</code>
option specifies the maximum number of characters per line, and
<code class="literal">subindent</code> specifies the indenting of text and
sub-elements, relative to the element's own tags. Note that when
<span><strong class="command">xmlformat</strong></span> performs line-wrapping, it includes the
currently prevailing indent as part of the line length. (For example, if
the prevailing indent is 20 spaces and <code class="literal">wrap-length</code>
value is <code class="literal">60</code>, lines will contain at most 40 characters
following the indentation.)
</p><p>
After adding the <code class="literal">para</code> section to
<code class="filename">xmlformat.conf</code>, <span><strong class="command">xmlformat</strong></span>
produces this result:
</p><pre class="screen">
&lt;example&gt;
 &lt;title&gt;Compiling and Running a Program&lt;/title&gt;
 &lt;para&gt;
  To compile and run the program, use the following
  commands, where
  &lt;replaceable&gt;source-file&lt;/replaceable&gt;
  is the name of the source file:
 &lt;/para&gt;
&lt;screen&gt;
&lt;userinput&gt;cc&lt;/userinput&gt; &lt;replaceable&gt;source-file&lt;/replaceable&gt;
&lt;userinput&gt;./a.out&lt;/userinput&gt;
&lt;/screen&gt;
&lt;/example&gt;
</pre><p>
The paragraph now is wrapped and indented. However, it doesn't seem to
be wrapped <span class="emphasis"><em>quite</em></span> correctly, because the
<code class="literal">&lt;replaceable&gt;</code> element actually would fit on the
previous line. This happens because no formatting options were specified
for <code class="literal">&lt;replaceable&gt;</code> in the configuration file. As
a result, it is treated as having the default element type of
<code class="literal">block</code>, using the default behavior that block elements
are written out beginning on a new line.
</p><p>
To fix this problem, we should configure
<code class="literal">&lt;replaceable&gt;</code> as an inline element. That will
cause it to be formatted inline with the other text (and thus
line-wrapped along with it). Modify the configuration file to include a
<code class="literal">replaceable</code> section: this:
</p><pre class="screen">
para
  format = block
  normalize = yes
  wrap-length = 60
  subindent = 1

replaceable
  format = inline

screen
  format = verbatim
</pre><p>
The resulting output after making this change is as follows:
</p><pre class="screen">
&lt;example&gt;
 &lt;title&gt;Compiling and Running a Program&lt;/title&gt;
 &lt;para&gt;
  To compile and run the program, use the following
  commands, where &lt;replaceable&gt;source-file&lt;/replaceable&gt; is
  the name of the source file:
 &lt;/para&gt;
&lt;screen&gt;
&lt;userinput&gt;cc&lt;/userinput&gt; &lt;replaceable&gt;source-file&lt;/replaceable&gt;
&lt;userinput&gt;./a.out&lt;/userinput&gt;
&lt;/screen&gt;
&lt;/example&gt;
</pre><p>
We're getting close now. All we need to do is space out the
<code class="literal">&lt;example&gt;</code> child elements with a blank line in
between. Sub-element spacing is controlled by three formatting
properties:
</p><div class="itemizedlist"><ul type="disc"><li><p>
<code class="literal">entry-break</code> controls spacing after the opening tag of
an element (that is, the spacing upon entry into the element's content).
</p></li><li><p>
<code class="literal">element-break</code> controls the spacing between
sub-elements.
</p></li><li><p>
<code class="literal">exit-break</code> controls spacing before the closing tag of
an element (that is, the spacing upon exit from the element's content).
</p></li></ul></div><p>
The value for each of these formatting options should be an integer
indicating the number of newlines to write. A value of
<code class="literal">1</code> causes one newline, which acts simply to break to
the next line. To get a blank line, the break value needs to be
<code class="literal">2</code>. Modify the configuration file by adding a section
for <code class="literal">&lt;example&gt;</code> elements:
</p><pre class="screen">
example
  format = block
  entry-break = 2
  element-break = 2
  exit-break = 2
  subindent = 0

para
  format = block
  normalize = yes
  wrap-length = 60
  subindent = 1

replaceable
  format = inline

screen
  format = verbatim
</pre><p>
The resulting output is:
</p><pre class="screen">
&lt;example&gt;

&lt;title&gt;Compiling and Running a Program&lt;/title&gt;

&lt;para&gt;
 To compile and run the program, use the following commands,
 where &lt;replaceable&gt;source-file&lt;/replaceable&gt; is the name of
 the source file:
&lt;/para&gt;

&lt;screen&gt;
&lt;userinput&gt;cc&lt;/userinput&gt; &lt;replaceable&gt;source-file&lt;/replaceable&gt;
&lt;userinput&gt;./a.out&lt;/userinput&gt;
&lt;/screen&gt;

&lt;/example&gt;
</pre><p>
We're done!
</p><p>
You may be thinking, "Wow, that's a lot of messing around just to format
that tiny little document." That's true. However, the effort of setting
up configuration files tends to be "reusable," in the sense that you can
use the same file to format multiple documents that all should be
written using the same style. Also, if you have different projects
requiring different styles, it tends to be easiest to begin setting up
the configuration file for one project by beginning with a copy of the
file from another project.
</p></div><div class="sect1" lang="en"><div class="titlepage"><div><div><h2 class="title" style="clear: both"><a name="inherited-formatting"></a>4. 
Discovering "Inherited" Formatting Options
</h2></div></div></div><p>
In the final formatting of <code class="filename">doc2.xml</code>, note that the
paragraph tags appear on separate lines preceding and following the
paragraph content. This occurs despite the fact that the configuration
file specifies no break values in the <code class="literal">para</code> section,
because if you omit formatting options for an element, it "inherits" the
default properties. In the case of the <code class="literal">&lt;para&gt;</code>
element, the relevant unspecified properties are the
<code class="literal">entry-break</code> and <code class="literal">exit-break</code> values.
For block elements, both have a value of <code class="literal">1</code> by default
(that is, one newline), which causes a line break after the opening tag
and before the closing tag.
</p><p>
If you want to see all the formatting options
<span><strong class="command">xmlformat</strong></span> will use, run it with the
<code class="option">--show-config</code> option. For example:
</p><pre class="screen">
% <strong class="userinput"><code>xmlformat --show-config</code></strong>
*DEFAULT
  format = block
  entry-break = 1
  element-break = 1
  exit-break = 1
  subindent = 1
  normalize = no
  wrap-length = 0

*DOCUMENT
  format = block
  entry-break = 0
  element-break = 1
  exit-break = 1
  subindent = 0
  normalize = no
  wrap-length = 0

example
  format = block
  entry-break = 2
  element-break = 2
  exit-break = 2
  subindent = 0
  normalize = no
  wrap-length = 0

para
  format = block
  entry-break = 1
  element-break = 1
  exit-break = 1
  subindent = 1
  normalize = yes
  wrap-length = 60

replaceable
  format = inline

screen
  format = verbatim
</pre><p>
No configuration file is specified on the command line, so
<span><strong class="command">xmlformat</strong></span> reads the default configuration file,
<code class="filename">xmlformat.conf</code>. Then it displays the resulting
configuration options. You can see that the <code class="literal">para</code>
section has inherited break values from the <code class="literal">*DEFAULT</code>
section.
</p></div><div class="sect1" lang="en"><div class="titlepage"><div><div><h2 class="title" style="clear: both"><a name="unconfigured-elements"></a>5. 
Checking for Unconfigured Elements
</h2></div></div></div><p>
Any elements appearing in the input document that are not named in the
configuration file are formatted using the values of the
<code class="literal">*DEFAULT</code> section. If the file contains no
<code class="literal">*DEFAULT</code> section, <span><strong class="command">xmlformat</strong></span> uses
built-in default values.
</p><p>
If you want to see whether there are any elements in the document for
which you haven't specified any formatting options, run
<span><strong class="command">xmlformat</strong></span> with the
<code class="option">--show-unconfigured-elements</code> option. For example:
</p><pre class="screen">
% <strong class="userinput"><code>xmlformat --show-unconfigured-elements doc2.xml</code></strong>
The following document elements were assigned no formatting options:
title
</pre><p>
As it happens, the title already formats in the desired fashion, so
there's no necessity of adding anything more to the configuration file.
</p></div></div></body></html>
