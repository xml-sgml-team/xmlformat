all:

# Execute this target after "svn export" into a new directory and before
# packaging the directory for distribution.

dist-prep::
	(cd docs;make dist-prep)

clean::
	(cd tests;make clean)
	(cd docs;make clean)

test::
	@echo Test Ruby version...
	./runtest -r all
	@echo Test Perl version...
	./runtest -p all
